/*
 * Copyright © 2019
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Alessandro Pozone <alessandro.pozone@hotmail.com>
 */

#include "./../mango_cl.h"
#include <host/mango.h>
#include <host/mango_types.h>
#include "./cl_types.h"
#include "./../mango_cl_platform.h"
#include <host/kernel_arguments.h>
#include <fstream>
#include <unistd.h>
#define GetCurrentDir getcwd

extern "C" {

// 1. clGetPlatformIDs: Returns a list of OpenCL platforms found in platforms
//https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clGetPlatformIDs.html
cl_int clGetPlatformIDs(cl_uint num_entries,
                        cl_platform_id *platforms,
                        cl_uint *num_platforms) {

    if (platforms == NULL && num_platforms == NULL)
        return CL_INVALID_VALUE;
    if (num_entries == 0 && platforms != NULL)
        return CL_INVALID_VALUE;
    if (num_platforms != NULL)
        *num_platforms = 1;
    if (platforms != NULL) {
        _cl_platform_id platformId = {"Mango Platform"};
        *platforms = &platformId;
    }

    return CL_SUCCESS;
}

// 2. clGetDeviceIDs: Returns a list of OpenCL devices found in devices given a platform
//https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clGetDeviceIDs.html
cl_int clGetDeviceIDs(cl_platform_id platform,
                      cl_device_type device_type,
                      cl_uint num_entries,
                      cl_device_id *devices,
                      cl_uint *num_devices) {

    //TODO expose all accelerators supported by Mango
    if (num_devices)
        *num_devices = 1;
    if (devices) {
        auto dev = new _cl_device_id();
        dev->type = mango::UnitType::GN;
        *devices = {dev};
    }
    return CL_SUCCESS;
}

//3. clCreateContext: An OpenCL context is created with one or more devices.
//Contexts are used by the OpenCL runtime for managing objects such as command-queues, memory, program and kernel
//objects and for executing kernels on one or more devices specified in the context.
//https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clCreateContext.html
cl_context clCreateContext(const cl_context_properties *properties,
                           cl_uint num_devices,
                           const cl_device_id *devices,
                           void (CL_CALLBACK *pfn_notify)(const char *errinfo,
                                                          const void *private_info,
                                                          size_t cb,
                                                          void *user_data),
                           void *user_data,
                           cl_int *errcode_ret) {

    if (user_data == NULL) {
        *errcode_ret = CL_INVALID_ARG_VALUE;
        return NULL;
    }

    //Mango Initialization
    mango::mango_init_logger();
    auto mango_rt = new mango::BBQContext(((cl_mango_data *) user_data)->application_name, ((cl_mango_data *) user_data)->recipe);
    auto ctx = new _cl_context();
    /* Save the user callback and user data*/
    ctx->mangoContext = mango_rt;
    ctx->pfn_notify = pfn_notify;
    ctx->user_data = user_data;
    ctx->buffer_counter = 1;
    ctx->buffers = new std::list<cl_mem>();
    ctx-> kernels = new std::list<cl_kernel>();

    return {ctx};
}

//4. clCreateCommandQueue: https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/clCreateCommandQueue.html
cl_command_queue clCreateCommandQueue(cl_context context,
                                      cl_device_id device,
                                      cl_command_queue_properties properties,
                                      cl_int *errcode_ret) {

    auto commandQueue = new _cl_command_queue();
    commandQueue->context = context;
    return commandQueue;
}

cl_program clCreateProgramWithSource(cl_context context,
                                     cl_uint count,
                                     const char **strings,
                                     const size_t *lengths,
                                     cl_int *errcode_ret) {

    auto program = new _cl_program();
    cl_int err = CL_SUCCESS;
    cl_uint i;

    if (context == NULL)
        err = CL_INVALID_CONTEXT;

    if (count == 0)
        err = CL_INVALID_ARG_VALUE;

    if (strings == NULL)
        err = CL_INVALID_ARG_VALUE;

    for (i = 0; i < count; i++) {
        if (strings[i] == NULL) {
            err = CL_INVALID_VALUE;
        }
    }

    if (err != CL_SUCCESS)
        return NULL;

    int32_t *lens = NULL;
    int32_t len_total = 0;
    char *p = NULL;

    lens = (int32_t *) calloc(count, sizeof(int32_t));
    for (i = 0; i < (int) count; ++i) {
        size_t len;
        if (lengths == NULL || lengths[i] == 0)
            len = strlen(strings[i]);
        else
            len = lengths[i];
        lens[i] = len;
        len_total += len;
    }

    auto source = (char *) calloc(len_total + 1, sizeof(char));
    program->source = source;
    p = source;
    for (i = 0; i < (int) count; ++i) {
        memcpy(p, strings[i], lens[i]);
        p += lens[i];
    }
    *p = '\0';

    program->source_type = mango::FileType::STRING;
    program->kernel_counter = 0;
    context -> program = program;

    return {program};
}

cl_program clCreateProgramWithBinary(cl_context context,
                                     cl_uint num_devices,
                                     const cl_device_id *device_list,
                                     const size_t *lengths,
                                     const unsigned char **binaries,
                                     cl_int *binary_status,
                                     cl_int *errcode_ret) {
    auto program = new _cl_program();
    cl_int err = CL_SUCCESS;
    cl_uint i;

    if (context == NULL)
        err = CL_INVALID_CONTEXT;

    if (num_devices == 0)
        err = CL_INVALID_ARG_VALUE;

    for (i = 0; i < num_devices; i++) {
        if (binaries[i] == NULL) {
            err = CL_INVALID_VALUE;
        }
    }

    if (err != CL_SUCCESS)
        return NULL;

    program->source = (char *) binaries[0];
    program->source_type = mango::FileType::BINARY;
    program -> unit_type = device_list[0]->type;
    program->kernel_counter = 0;
    context -> program = program;

    return {program};
}

cl_program clCreateProgramWithBuiltInKernels(cl_context context,
                                             cl_uint num_devices,
                                             const cl_device_id *device_list,
                                             const char *kernel_names,
                                             cl_int *errcode_ret) {
    printf("Mango Error: Unsupported method");
    return NULL;
}

cl_program clCreateProgramWithIL(cl_context context,
                                 const void *il,
                                 size_t length,
                                 cl_int *errcode_ret) {
    printf("Mango Error: Unsupported method");
    return NULL;
}

std::string get_current_dir() {
    char buff[FILENAME_MAX]; //create string buffer to hold path
    GetCurrentDir( buff, FILENAME_MAX );
    return std::string(buff);
}

cl_int clBuildProgram(cl_program program,
                      cl_uint num_devices,
                      const cl_device_id *device_list,
                      const char *options,
                      void (CL_CALLBACK *pfn_notify)(cl_program program,
                                                     void *user_data),
                      void *user_data) {

    if(program->source_type == mango::FileType::BINARY)
        return CL_SUCCESS;

    if(num_devices == 0)
        return CL_INVALID_ARG_VALUE;

    if(!device_list)
        return CL_INVALID_ARG_VALUE;

    for(int i = 0; i < num_devices; i++) {
        {
            std::string arch = device_list[i]->type == mango::UnitType::GN ? "GN" : "PEAK";

            if (!options)
                options = "";

            std::string optionsString(options);
            std::ofstream outfile("source.c");
            outfile << program->source << std::endl;
            outfile.close();

            std::string script = "#/bin/bash\n"
                                 "echo \"Building kernel in: $PWD\"\n"
                                 "MANGO=\"/opt/mango\"\n"
                                 "INPUTFILE=\"source.c\"\n"
                                 "ARCH=\"" + arch + "\"\n"
                                                    "$MANGO/bin/mango_gen_kernel_entry.py $ARCH $PWD/$INPUTFILE\n"
                                                    "gcc main.c $INPUTFILE -I$MANGO/include/libmango -L$MANGO/lib -lmango-dev-gn -pthread " + optionsString + " -o kernel\n"
                                                                                                                                                              "echo \"Kernel build was successful\"";
            system(script.c_str());
            auto path = strdup(get_current_dir().c_str());

            program->source = strcat((char *) path, "/kernel");
            program->source_type = mango::FileType::BINARY;
            program -> unit_type = device_list[i]->type;
        }
    }

    return CL_SUCCESS;
}



cl_int clGetProgramBuildInfo(cl_program program,
                             cl_device_id device,
                             cl_program_build_info param_name,
                             size_t param_value_size,
                             void *param_value,
                             size_t *param_value_size_ret) {
    return NULL;
}

cl_kernel clCreateKernel(cl_program program,
                         const char *kernel_name,
                         cl_int *errcode_ret) {

    auto kernel = new _cl_kernel();

    auto kef = new mango::KernelFunction();
    kef -> load(program->source,program->unit_type, program->source_type);
    kernel->function = kef;

    program -> kernel_counter++;
    kernel-> id = program -> kernel_counter;

    kernel -> arguments = new std::list<mango::Arg*>{};
    return {kernel};
}

cl_mem clCreateBuffer(cl_context context, cl_mem_flags flags, size_t size, void *host_ptr, cl_int *errcode_ret) {
    auto mem = new _cl_mem();

    mem->size = size;
    mem->buffer_id = context->buffer_counter;
    mem->kernels_out = new std::vector<mango::mango_id_t>();
    mem->kernels_in = new std::vector<mango::mango_id_t>();
    mem -> content = host_ptr;

    context->buffers->push_front(mem);
    context-> buffer_counter++;

    return {mem};
}

cl_int clSetKernelArg(cl_kernel kernel,
                      cl_uint arg_index,
                      size_t arg_size,
                      const void *arg_value) {
    if ( arg_size == sizeof(cl_mem))
    {
        auto cl = *(cl_mem*) arg_value;
        auto buffer = new mango::CLMemArg(cl->buffer_id);
        kernel->arguments->push_back(buffer);
    }
    else
    {
        auto scalar = new mango::ScalarArg<int>(*(int*)arg_value);
        kernel->arguments->push_back(scalar);
    }
    return CL_SUCCESS;
}

cl_int clEnqueueNDRangeKernel(cl_command_queue command_queue,
                              cl_kernel kernel,
                              cl_mem *in_buffers,
                              cl_uint num_in_buffers,
                              cl_mem *out_buffers,
                              cl_uint num_out_buffers,
                              cl_uint num_events_in_wait_list,
                              cl_event *event_wait_list,
                              cl_event *event) {

    auto mango_context = command_queue->context->mangoContext;
    auto context = command_queue->context;

    auto in_buf = new  std::vector<mango::mango_id_t>();
    for(int i = 0; i < num_in_buffers; i++){
        auto item = in_buffers[i];
       in_buf-> push_back(item->buffer_id);
       item->kernels_out->push_back(kernel->id);
    }

    auto out_buf = new std::vector<mango::mango_id_t>();
    for(int i = 0; i < num_out_buffers; i++){
        auto item = out_buffers[i];
        out_buf->push_back(item->buffer_id);
        item->kernels_in->push_back(kernel->id);
    }

    auto program = command_queue->context-> program;
    //Register the kernel
    kernel -> kernel = mango_context->register_kernel(program->kernel_counter, kernel->function, *in_buf, *out_buf);
    context->kernels->push_front(kernel);

    return NULL;
}

cl_int clStartComputation(cl_command_queue command_queue){
    auto context = command_queue->context;
    auto mango_context = command_queue->context->mangoContext;

    auto tg = new mango::TaskGraph();

    for(auto ker : *context-> kernels){
        auto kernel = (ker->kernel);
        *tg += kernel;
        *tg += kernel->get_termination_event();
        for(const auto e : kernel->get_task_events())
            *tg += e;
    }

    for(auto mem : *context->buffers){
        auto buf = mango_context-> register_buffer( std::shared_ptr<mango::Buffer> (new mango::FIFOBuffer(mem->buffer_id, mem->size, *mem->kernels_in, *mem->kernels_out)), mem->buffer_id);
        if(mem-> content != nullptr)
            buf->write(mem->content, mem->size);
        *tg += buf;
        *tg += buf->get_event();
    }

    // Resource Allocation
    mango_context->resource_allocation(*tg);
    command_queue->context->program->graph = tg;

    for(auto ker : *context-> kernels) {
        auto argsVector = new std::vector<mango::Arg *>();

        for (auto arg : *(ker->arguments)) {
            if (typeid(*arg) == typeid(mango::CLMemArg)) {
                auto buffer = mango_context->get_buffer(arg->get_value());
                argsVector->push_back(static_cast<mango::Arg *>(new mango::BufferArg(buffer)));
            }
            else if (typeid(*arg) == typeid(mango::CLBufArg)) {
                auto argt = static_cast<mango::CLBufArg *>(arg);
                auto buffer = mango_context->get_buffer(arg->get_value());
                buffer->read(argt->get_destination(), argt->get_size());
                argsVector->push_back(static_cast<mango::Arg *>(new mango::EventArg(buffer->get_event())));
            }
            else {
                argsVector->push_back(arg);
            }
        }

        auto args = new mango::KernelArguments(*argsVector, (ker->kernel));

        auto e = mango_context->start_kernel((ker->kernel), *args);
        e->wait();
    }
}

cl_int clEnqueueReadBuffer(cl_command_queue command_queue,
                           cl_mem buffer,
                           cl_bool blocking_read,
                           size_t offset,
                           size_t size,
                           void *ptr,
                           cl_uint num_events_in_wait_list,
                           const cl_event *event_wait_list,
                           cl_event *event) {

    auto kernel = *(command_queue->context->kernels->begin());
    kernel->arguments->push_back(static_cast<mango::Arg *>(new mango::CLBufArg(buffer->buffer_id, ptr, size)));
    return NULL;
}


cl_int clReleaseMemObject(cl_mem memobj) {
    delete memobj;
    return CL_SUCCESS;
}

cl_int clReleaseProgram(cl_program program) {
    delete program;
    return CL_SUCCESS;
}

cl_int clReleaseKernel(cl_kernel kernel) {
    delete kernel;
    return CL_SUCCESS;
}

cl_int clReleaseCommandQueue(cl_command_queue command_queue) {
    delete command_queue;
    return CL_SUCCESS;
}

cl_int clReleaseContext(cl_context context) {
    context ->mangoContext ->resource_deallocation(*(context->program->graph));
    delete context;
    return CL_SUCCESS;
}

}

