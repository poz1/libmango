/*
 * Copyright © 2019
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Alessandro Pozone <alessandro.pozone@hotmail.com>
 */
#include <host/mango_types_c.h>
#include <host/mango.h>
#include <host/mango_types.h>
#include <list>

#ifndef OPENCL_CL_PROGRAM_H
#define OPENCL_CL_PROGRAM_H

#endif //OPENCL_CL_PROGRAM_H

struct cl_mango_data {
    char * application_name;
    char * recipe;
};

struct _cl_platform_id {
  char * name;
};


struct _cl_device_id  {
    mango::UnitType type;
};

struct _cl_context {
    mango::BBQContext* mangoContext;
    cl_device_id* devices;            /* All devices belong to this context */
    cl_uint device_num;               /* Devices number of this context */

    _cl_program* program;
    mango::mango_id_t buffer_counter;

    std::list<cl_mem>* buffers;
    std::list<cl_kernel>* kernels;
    void (CL_CALLBACK *pfn_notify)(const char *, const void *, size_t, void *);
    void *user_data;                   /* A pointer to user supplied data */
};

struct _cl_program {
    mango::TaskGraph* graph;
    mango::KernelFunction* kernel;
    char *source;
    mango::FileType source_type;
    mango::UnitType unit_type;

    mango::ExitCode result;
    mango::mango_id_t kernel_counter;
};

struct _cl_kernel{
    mango::mango_id_t id;
    std::shared_ptr<mango::Kernel> kernel;
    mango::KernelFunction* function;
    std::list<mango::Arg*>* arguments;
};

struct _cl_command_queue{
    cl_context context;
};

struct _cl_mem{
    mango::mango_id_t buffer_id;
    mango::mango_size_t size;
    std::vector<mango::mango_id_t>* kernels_in;
    std::vector<mango::mango_id_t>* kernels_out;
    void* content;
};

namespace mango {
    class CLMemArg : public ScalarArg<mango::mango_id_t >::ScalarArg {
    public:
        CLMemArg(mango_buffer_t i) : ScalarArg(i) {

        }
    };

    class CLBufArg : public ScalarArg<mango::mango_id_t >::ScalarArg {
    public:
        CLBufArg(mango_buffer_t i, void* destination, size_t size) : ScalarArg(i) {
            this->destination = destination;
            this-> size = size;
        }
        void* get_destination(){
            return this->destination;
        }
        size_t get_size(){
            return this-> size;
        }
    private:
        void* destination;
        size_t size;
    };
}